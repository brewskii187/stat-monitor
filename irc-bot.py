#!/usr/bin/env python

# bot commands are BOTNAME server-health and BOTNAME quit
# Server health will query all remote servers and print
# their stats to the IRC server. There is also a web-query.php
# file that you can use.

from __future__ import print_function
import socket, random, re, os, time
from datetime import datetime


host = 'your.ircserver.com'  # irc server were sending the bot into
port = 6667  # port of the irc server
botName = 'Stats-bot'  # the bots nick
channel = '#someChannel'  # channel the bots going in
channelPass = ''  # if the channel has a pass put it here.
queryServers = [('123.123.123.132', 41286),  # List of touples with the ip/port (str,int)
   ('321.321.321.321', 41286)]
allowedHostnames = ('your.hostname', 'other.hostname')  # host names that are allowed to kill the bot.

# welcome and part messages to users coming and going.

# Welcome Nick have a _____ day!'
welcomes = ['wonderful', 'splendid', 'kablowsiony', 'awesome', 'absolute', 'admirable', 'awesome', 'bad ass', 'best', 'exceptional', 'fantastic', 'fine', 'good', 'marvelous']

parts = ['what are we gonna do without you!', 'hurry back!', 'ill miss you', 'I hate to see you go']


#
# end config
#
actions = ('part', 'join', 'privmsg', 'nick')
cdir = os.getcwd()
listDir = os.listdir(cdir)
now = str(int(time.time()))

if 'logs' not in listDir:
    os.mkdir('logs', 0770)
    print("made logs directoy")
else:
    print("logs directoy already exists")

fd = file('logs/log_' + now + '.txt', 'w+')

def log(msg):
    global fd
    print("logging")
    print(str(datetime.now()) + ": " + msg + "\n", file=fd)





def SendCmd(cmd):
    cmd += '\n\r'
    s.send(cmd, len(cmd))
    print ("sent :" + cmd)
    return


# returns a dictionary sorta like this
#{'action': 'privmsg', 'msg': 'Stats-bot server-health', 'hostname': 'nineteen.eleven', 'userName': 'Nineteen', 'nick': 'Nineteen'}
def breakdown(read_buffer):
    print(read_buffer)

    global channel

    buffer_list = {}
    #s = read_buffer.find(':')
    #if s is 0:
        #read_buffer = read_buffer[1:]
    e = read_buffer.find('\r\n')
    if e is not -1:
        read_buffer = read_buffer[:e]

    read_buffer = re.sub(r"^\W+", '', read_buffer)
    lower_buffer = read_buffer.lower()

    for a in actions:  # look for actions we want to break down
        actionJackson = lower_buffer.find(a)

        if actionJackson is not -1:
            buffer_list['action'] = a
            s = read_buffer.find(':')
            buffer_list['msg'] = read_buffer[s:]
            read_buffer = read_buffer[:s]
                #read_buffer, buffer_list['msg'] = read_buffer.split(":")
            break

    if buffer_list.has_key('action') is False:  # if action was not assigned get out
        return False

    if (buffer_list.has_key('msg')):
        if(buffer_list['msg'] is channel):
            del buffer_list['msg']
        else:
            buffer_list['msg'] = re.sub(r"^\W+", '', buffer_list['msg'])

    nickNum = read_buffer.find('!')  # get the position of the irc users name
    buffer_list['nick'] = read_buffer[:nickNum]  # extract the irc users name

    userNum = read_buffer.find('@')
    buffer_list['userName'] = read_buffer[nickNum + 1:userNum]

    hostNum = lower_buffer.find(buffer_list['action'])
    buffer_list['hostname'] = read_buffer[userNum + 1:hostNum - 1]

    return buffer_list

# returns True or Flase if host name is allowed.
def checkHostname(hostname):
    global allowedHostnames
    allowedHost = False

    for hs in allowedHostnames:
        if hs == hostname:
            allowedHost = True
            break
    return allowedHost


s = socket.socket()  # set up the socket
s.connect((host, port))  # connect to the irc server

SendCmd("PASS NOPASS")
SendCmd("NICK " + botName)
SendCmd("USER py-bot USING NaNtEnnPython BOT")
print("sent commands")
while True:
    read_buffer = s.recv(1024)
    log(read_buffer)
    #print read_buffer
    if (read_buffer[:6] == 'PING :'):
        SendCmd('PONG :' + read_buffer[6:])
        #print('Replied to ping')
    elif("422" in read_buffer):
        if channelPass:
            SendCmd('JOIN ' + channel + ' ' + channelPass)
        else:
            SendCmd('JOIN ' + channel)
            joined = True
    else:

        bd = breakdown(read_buffer)
# if its not something were looking for skip all that code under here
        if bd is False:
            continue

# query all the game servers and get their health reports
        if botName + ' server-health' in bd['msg']:
            for server in queryServers:  # loop through our list of servers
                health = socket.socket()  # Set up the socket
                try:
                    health.connect(server)  # connect to our remote server
                except socket.error as msg:
                    print ("Unable to connect to " + host + ", " + str(msg))
                else:
                    health.send('data')  # tell remote server what to do
                    data = health.recv(1024)  # revieve the data from the server
                    lines = data.split('\n')  # chop the string into a list at linebreaks
                    for line in lines:  # loop though and send  to IRC
                        SendCmd('PRIVMSG ' + channel + ' :' + line)
                        time.sleep(.5)  # sleep so the irc server doesnt flag us for flooding
                    SendCmd('PRIVMSG ' + channel + ' :-------------------------')
                    health.close()  # close the connection

# welcome users to the channel
        if 'join' in bd['action']:
            if 'nick' in bd:
                if botName not in bd['nick']:
                    adjective = random.choice(welcomes)
                    SendCmd('PRIVMSG ' + channel + ' :Welcome ' + bd['nick'] + ' have a ' + adjective + ' day!')  # welcome them to the server

# welcome users to the channel
        if 'part' in bd['action']:
            if 'nick' in bd:
                if botName not in bd['nick']:
                    partMsg = random.choice(parts)
                    SendCmd('PRIVMSG ' + channel + ' : ' + bd['nick'] + ', ' + partMsg)  # welcome them to the server

# if one of the allowed hostnames kills  us do it.
        if botName + ' quit' in bd['msg']:
            if checkHostname(bd['hostname']):
                s.close()
                os.close(fd)
                raise SystemExit
            else:
                 # if it was an imposter, tell them no
                SendCmd('PRIVMSG ' + channel + ' :I cant let you do that Michael')

        print (bd)
