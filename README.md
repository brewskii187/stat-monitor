### This is an IRC bot and remote monitoring tool for other servers ###

### Requirements ###
1. Python
2. psutil Python module

### Optional ###
1. Web server with PHP

### The bot commands are ### 
* BOTNAME server-health
* BOTNAME quit

Server health will query all remote servers and print their stats to the IRC server. There is also a web-query.php file that you can use.