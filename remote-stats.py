#!/usr/bin/env python

# This file goes on one of the remote servers you want to monitor

import os, psutil, time, socket
from time import sleep

sleepTimer = 2  # how long to get CPU and network information for


def convertBytes(bytes):
    if bytes <= 1024:
        return str(bytes) + 'bytes'

    if bytes >= 1024 and bytes < 1048576:
        return str(float(round(bytes / 1024.0, 2))) + 'kb'

    if bytes >= 1048576 and bytes < 1073741824:
        return str(float(round(bytes / 1048576.0, 2))) + 'mb'

    if bytes >= 1073741824:
        return str(float(round(bytes / 1073741824.0, 2))) + 'gb'

s = socket.socket()         # Create a socket object

host = ''                   # Bind all addresses
port = 41286                # Port were gonna use
s.bind((host, port))        # Bind to the port


while True:
    print "Waiting for connections \n"
    s.listen(5)                 # Now wait for client connection.
    c, addr = s.accept()              # Establish connection with client.
    print "connected to " + str(addr) + "\n"

    recv = c.recv(1024)
    if recv:
        if 'quit' in recv:
            c.close()
            raise SystemExit
    p = psutil.Process(os.getpid())  # Set psuitl to read OS information

    mem = psutil.virtual_memory()
    swap = psutil.swap_memory()

    net1 = psutil.net_io_counters()  # start the net IO counter
    psutil.cpu_percent(interval=0)  # start the CPU percentage counter
    time.sleep(sleepTimer)
    cpuUsage = psutil.cpu_percent(interval=0)  # stop the CPU percentage counter
    net2 = psutil.net_io_counters()  # stop the net IO counter
    net_sent = (net2[0] - net1[0])/sleepTimer  # get the difference between the 2 counters
    net_rec = (net2[1] - net1[1])/sleepTimer  # get the difference between the 2 counters
    cpu = "CPU: " + str(cpuUsage)

    # send the information over the socket
    c.send("Stats for " + socket.getfqdn() + "\n" +
    'Avail. Mem: ' + convertBytes(mem[1]) + '\n' +
    'Avail. Swap: ' + convertBytes(swap[2]) + '\n' +
    cpu + '\n' +
    'Bytes/sec sent: ' + convertBytes(net_sent) + '\n' +
    'Bytes/sec recieved: ' + convertBytes(net_rec) + '\n')
    c.close()  # close the connection
    print "Sent data, closed connection\n"
